from django.urls import path
from receipts.views import (
    home,
    receipt_create,
    category_list,
    account_list,
    category_create,
    account_create,
)

urlpatterns = [
    path("", home, name="home"),
    path("create/", receipt_create, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", category_create, name="create_category"),
    path("accounts/create/", account_create, name="create_account"),
]
